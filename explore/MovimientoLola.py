#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import math
import serial

#Correccion de inercia debido a que al parar se va una distancia
CORRECCION_INERCIA=0

def avanzar(ser,ang0,x0,y0,movimiento):
    #variables inicio posicion
    theta = math.radians(ang0)    
    print("Angulo inicial en grados = {}".format(ang0))
    print("Angulo inicial en radianes = {}".format(theta))
    #variables calculo movimiento
    #Avance lineal por pulso del encoder
    cm = 0.2094
    #Nuestro diferencial de movimiento lo asociamos a delta numero de pulsos del encoder
    Delta = 15
    #Longitud del eje entre ruedas (mm)
    L = 190
    x = x0
    y = y0
    ValorD_delta = 0
    ValorI_delta = 0
    ValorD=0
    ValorI=0
    sR,sC,sL = 0,0,0
    theta_delta = 0
    derAnt=0
    izqAnt=0
    pulsosDER, pulsosIZQ = 0,0
    distanciaDER = 0
    auxDER,auxIZQ=0,0
    #variable para verificar que a parado
    iguales=0

    #Comenzamos mandando la orden (velocidades distintas) y leyendo los encoder
    ser.write('1125135'.encode('ascii'))
    ser.write(';'.encode('ascii'))
    valor=ser.readline().decode('ascii')
    #print("Leyendo encoders por prmera vez")

    #Hasta que no tengamos una lectura clara, leyemos el puerto serie solicitando lectura de encoders 
    #lectura de encoders es el char ';'
    while (valor[0:4]!=')0*0'):
        ser.write(';'.encode('ascii'))
        valor=ser.readline().decode('ascii')


    while (ValorD<=round(4.775*movimiento)) and (ValorI<=round(4.775*movimiento)):
        ser.write(';'.encode('ascii'))
        valor=ser.readline().decode('ascii')
        #Condicion para que si el primer carácter recibido no es el principio de la trama que queremos, empezamos de nuevo 
        #el bucle while ignorando el resto del código (sentencia continue)
        if (valor[0]!=')'):     
            continue

        #Deacuerdo al formato recibido, los valores se separan por el *
        ValorII,ValorDD=valor[1:-2].split('*')
        #ValorI_anterior y ValorD_anterior son los pulsos de los encoder. Fijamos la distancia delta
        ValorI=int(ValorII)
        ValorD=int(ValorDD)

        #Si hemos recorrido la distancia delta definida: 
        if ValorI >= ValorI_delta and ValorD >= ValorD_delta:
            auxDER = ValorD - pulsosDER
            auxIZQ = ValorI - pulsosIZQ

            pulsosDER = ValorD
            pulsosIZQ = ValorI
        
            sL = cm*auxIZQ
            sR = cm*auxDER
            #valor absoluto para no diferenciar si la desviación fue a izquierda o derecha
            theta_delta = (sR-sL)/L
            sC = (sR+sL)/2
            
            #Actualizamos las variables de la nueva posicion
            theta = theta + theta_delta
            x = x + sC*math.cos(theta)
            y = y - sC*math.sin(theta)

            ValorD_delta = ValorD + Delta
            ValorI_delta = ValorI + Delta

        
        #Si no hemos alcanzado esa distancia, no hacemos nada y volvemos al while para seguir leyendo los encoders
        else:
            continue

            

    #Paramos motores cuando hemos llegado al numero de pulsos necesarios para alcanzar el objetivo
    ser.write('?'.encode('ascii'))
    #time.sleep(1)

    #Comprobamos que durante 20 veces los encoder permanecen quietos
    while (iguales<20):
        ser.write(';'.encode('ascii'))
        valor=ser.readline().decode('ascii')
        if (valor[0]!=')'):
            continue

        #Deacuerdo al formato recibido, los valores se separan por el *
        ValorII,ValorDD=valor[1:-2].split('*')
        #ValorI_anterior y ValorD_anterior son los pulsos de los encoder. Fijamos la distancia delta
        ValorI=int(ValorII)
        ValorD=int(ValorDD)

        #Si hemos recorrido la distancia delta definida: 
        if ValorI >= ValorI_delta and ValorD >= ValorD_delta:
            auxDER = ValorD - pulsosDER
            auxIZQ = ValorI - pulsosIZQ

            pulsosDER = ValorD
            pulsosIZQ = ValorI
        
            sL = cm*auxIZQ
            sR = cm*auxDER
            #valor absoluto para no diferenciar si la desviación fue a izquierda o derecha
            theta_delta = (sR-sL)/L
            sC = (sR+sL)/2
            
            #Actualizamos las variables de la nueva posicion
            theta = theta + theta_delta
            x = x + sC*math.cos(theta)
            y = y - sC*math.sin(theta)

            ValorD_delta = ValorD + Delta
            ValorI_delta = ValorI + Delta


        if derAnt==ValorD and izqAnt == ValorI:
            #print("IGUALES")
            iguales+=1
        else:
            iguales=0
            derAnt=ValorD
            izqAnt=ValorI

        
            
    time.sleep(0.5)
    return (x,y,math.degrees(theta),ValorI,ValorD)


if __name__ == "__main__":

    try:
    
        #Inicializacion previa conexion serial
        ser=serial.Serial('/dev/ttyAMA0',9600,timeout=3)
        print ("Conectado")
        time.sleep(5)
        ser.write('<'.encode('ascii'))
        time.sleep(1)
        #Fin inicializacion
        i = 1
        X,Y,ANG,ValorI,ValorD = 500,100,90,0,0
        print("Coordenadas iniciales: X= {} Y = {} ANG = {}".format(X,Y,ANG))
        while i <= 1:
            #print("ENtramos al while")
            X,Y,ANG,ValorI,ValorD=avanzar(ser,ANG,X,Y,1000)
            print ("Coordenada X: {}   Coordenada Y: {}".format(X,Y))
            print ("Angulo: {}".format(ANG))
            print ("Valor encoder Izquierdo: {}  Derecho: {}".format(ValorI,ValorD))
            input("Presione Intro para continuar")
            i +=1
            print ("\n")

    except Exception as inst:
            #Informe error
            print (inst)
            #Fin informorme
    finally:
            #Cerrado y visualizacion
            ser.write('?'.encode('ascii'))
            time.sleep(0.5)
            ser.close()

