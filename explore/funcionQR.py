'''
Created on 27 feb. 2017

@author: pedro
'''

import cv2
import numpy
import QRPose
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import zbar
import Image

CORRECION_F=0
vecesQR=0

class QR():
    def __init__(self):
        #Raspberry cam.
        self.camera = PiCamera()
        self.camera.resolution=(1920, 1088)
        #self.camera.start_preview()
        self.camera.hflip=True
        self.camera.vflip=True
        self.rawCapture = PiRGBArray(self.camera)
        # allow the camera to warmup
        time.sleep(1)
        self.camera.capture(self.rawCapture, format="bgr")
        img=self.rawCapture.array
        self.rawCapture.truncate(0)
        tam=img.shape[0:2]    
        self.QRPosition=QRPose.QRCode(tam)
        f=0.72*tam[1]
        self.K=numpy.matrix([
            [f, 0, tam[1]/2],
            [0, f, tam[0]/2],
            [0, 0,        1]], numpy.float32)
        d=2.5
        self.objeto=numpy.matrix([
            [-d, -d, 0],
            [-d, +d, 0],
            [+d, +d, 0],
            [+d, -d, 0]], numpy.float32)

        self.scanner = zbar.ImageScanner()
        self.scanner.parse_config('enable')

    def localizar(self, archivo, archivo1):
        self.rawCapture.truncate(0)
        self.camera.capture(self.rawCapture, format="bgr")
        img=self.rawCapture.array
        vecesQR=0
        gray=cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
        try:
            p1=self.QRPosition.FindQRLines(gray)
            res, r1, t1=cv2.solvePnP(self.objeto, p1, self.K, None)
            R1, _=cv2.Rodrigues(r1)
            Rp1=numpy.matrix(R1.T)
            print ("QR ENCONTRADO\n")
            print (t1[0],r1[1],t1[2]-CORRECION_F)
            archivo.write(str(t1[0]).replace('[','').replace(']','')+","+str(r1[1]).replace('[','').replace(']','')+","+str(t1[2]-CORRECION_F).replace('[','').replace(']','')+";...\n")
        
            pil = Image.fromarray(gray)
            width, height = pil.size
            raw = pil.tostring()

            image = zbar.Image(width, height, 'Y800', raw)
            self.scanner.scan(image)

            try:
                for symbol in image:
                    if vecesQR<1:
                        print (symbol.data+"\n")
                        archivo1.write(symbol.data+";...\n")
                        vecesQR=vecesQR+1
                    else:
                        pass
                        
            except:
                print ("NO SE PUEDE DECODIFICAR\n")
                archivo1.write("NoSePuedeDecodificar;...\n")

        except Exception as e: 
            archivo.write(str(9999)+","+str(9999)+","+str(9999)+";...\n")
            archivo1.write("NoSePuedeDecodificar;...\n")
            print ("QR NO ENCONTRADO\n")

        # clear stream for next frame
        self.rawCapture.truncate(0)

    def close(self):
        self.camera.close()
