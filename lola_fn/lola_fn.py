#!/usr/bin/python2
# -*- coding: utf-8 -*-

# Ui framework
import Tkinter as tk
import sys, os, time, pygame


class cuento:
    def __init__(self, lola):
        # copy the lola object to self.lola, so it can be used later
        self.lola = lola

        # get the program's PATH so we can find the picture and sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'
        image_file = self.path + "lola_fn/cuento.gif"

        # paint the screen
        self.image = tk.PhotoImage(file=image_file)
        self.canvas = tk.Canvas(self.lola.aux_windows[0], height=480, width=800, bg="blue")
        self.canvas.create_image(800 / 2, 480 / 2, image=self.image)

        # Bind the screen clic to the "handler" method
        self.canvas.bind("<Button-1>",  self.handler)

        # prepare the screen
        self.canvas.pack()

        # update the screen (what the uses sees)
        self.lola.aux_windows[0].update()

    def handler(self, event):
        pygame.mixer.music.stop()
        self.lola.aux_windows[0].destroy()

    def run(self, actions=None):
        #make sure the TTS is quiet
        while self.lola.synthesizer.IsSpeaking():
            pass
        # prepare the audio subsystem
        pygame.mixer.init()
        pygame.mixer.music.load(self.path + 'lola_fn/cuento.ogg')

        # start playing the audio
        pygame.mixer.music.play()

        # Wait until there is no audio playing
        while pygame.mixer.music.get_busy():
            self.lola.aux_windows[0].update()
            pygame.time.Clock().tick(10)
